#include <linux/sched.h>
#include <uapi/linux/ptrace.h>
#include <linux/fs.h>
#include <linux/blkdev.h>

// This hash maps each child process to its top-level gitaly child process
BPF_HASH(descendent);

struct process_activity_info_t {
    u64 read_bytes;
    u64 read_total;
    u64 written_bytes;
    u64 written_total;
    u64 block_reads;
    u64 block_writes;
};

// This hash maps each child process to its top-level gitaly child process
BPF_HASH(process_activity, u64, struct process_activity_info_t, MAXIMUM_HASH_SIZE);

#define MAXARG 10

struct top_level_execve_data {
    u32 pid;
    int argp;
    char argv[ARGSIZE];
};

// These events are used to signal new top-most child processes
BPF_PERF_OUTPUT(events);

// Returns the current PID
static u64 current_pid()
{
    return bpf_get_current_pid_tgid() >> 32;
}

static u64 current_parent_pid() {
    struct task_struct *task;

    task = (struct task_struct *)bpf_get_current_task();
    // Some kernels, like Ubuntu 4.13.0-generic, return 0
    // as the real_parent->tgid.
    // We use the get_ppid function as a fallback in those cases. (#1883)
    return task->real_parent->tgid;
}

// Given a PID, returns the top-most parent
static u64 find_top_level_pid(u64 pid)
{
    u64 *l = descendent.lookup(&pid);

    if(!l) { return 0; }

    return *l;
}

// Given a PID, returns true if it is the top-most child process
static u64 is_top_level_pid(u64 pid)
{
    u64 *top_pid_ptr = descendent.lookup(&pid);

    if(!top_pid_ptr) { return 0; }

    return *top_pid_ptr == pid;
}

TRACEPOINT_PROBE(sched, sched_process_exit) {
    u64 pid = current_pid();

    descendent.delete(&pid);
    // NOTE: Do not delete from `process_activity`, userland will do this once its read the data

    return 0;
}

static int __submit_arg(struct pt_regs *ctx, int argpos, void *ptr, struct top_level_execve_data *data) {
    bpf_probe_read(data->argv, sizeof(data->argv), ptr);
    data->argp = argpos;
    events.perf_submit(ctx, data, sizeof(struct top_level_execve_data));
    return 1;
}

static int submit_arg(struct pt_regs *ctx, int argpos, void *ptr, struct top_level_execve_data *data) {
    const char *argp = NULL;
    bpf_probe_read(&argp, sizeof(argp), ptr);
    if (argp) {
        return __submit_arg(ctx, argpos, (void *)(argp), data);
    }
    return 0;
}

static int submit_terminal_arg(struct pt_regs *ctx, struct top_level_execve_data *data) {
    data->argv[0] = 0;
    data->argp = -1;
    events.perf_submit(ctx, data, sizeof(struct top_level_execve_data));
    return 1;
}

static int current_task_is_gitaly() {
    char comm[TASK_COMM_LEN];
    bpf_get_current_comm(&comm, sizeof(comm));
    return comm[0] == 'g' && comm[1] == 'i' && comm[2] == 't' && comm[3] == 'a' && comm[4] == 'l' && comm[5] == 'y' && comm[6] == 0;
}

int syscall__execve(struct pt_regs *ctx,
    const char __user *filename,
    const char __user *const __user *__argv,
    const char __user *const __user *__envp) {
    u64 pid = current_pid();
    u64 ppid = current_parent_pid();

    if(current_task_is_gitaly()) {
        #ifdef PERF_TOOLS_DEBUG
        bpf_trace_printk("syscall__execve: top-level ppid=%d, pid=%d\n", ppid, pid);
        #endif

        descendent.update(&pid, &pid);
    } else {
        u64 topmost_pid = find_top_level_pid(ppid);
        if(!topmost_pid) {
            #ifdef PERF_TOOLS_DEBUG
            bpf_trace_printk("syscall__execve: ignoring unrelated spawn: ppid=%d, pid=%d\n", ppid, pid);
            #endif

            return 0;
        }

        #ifdef PERF_TOOLS_DEBUG
        bpf_trace_printk("syscall__execve: grandchild spawn ppid=%d, pid=%d\n", ppid, pid);
        #endif

        descendent.update(&pid, &topmost_pid);

        // Only continue for top children, not grandchildren
        return  0;
    }

    // Notify user-land of the command line args for the top

    struct top_level_execve_data data = {};
    data.pid = pid;

    __submit_arg(ctx, 0, (void *)filename, &data);

    // skip first arg, as we submitted filename
    #pragma unroll
    for (int i = 1; i < MAXARG; i++) {
        if (submit_arg(ctx, i, (void *)&__argv[i], &data) == 0) {
            submit_terminal_arg(ctx, &data);
            return 0;
        }
    }

    submit_terminal_arg(ctx, &data);
    return 0;
}

int trace_read_entry(struct pt_regs *ctx, struct file *file, char __user *buf, size_t count) {
    u64 pid = current_pid();


    // Is this read from a process we care about?
    u64 top_level_pid = find_top_level_pid(pid);
    if (!top_level_pid) { return 0; }

    struct process_activity_info_t *val, zero = {};
    zero.read_bytes = count;
    zero.read_total = 1;
    zero.written_bytes = 0;
    zero.written_total = 0;
    zero.block_reads = 0;
    zero.block_writes = 0;

    val = process_activity.lookup_or_init(&top_level_pid, &zero);
    if (val) {
        val->read_bytes += count;
        val->read_total++;
    }

    return 0;
}

int trace_write_entry(struct pt_regs *ctx, struct file *file, char __user *buf, size_t count) {
    u64 pid = current_pid();

    // Is this read from a process we care about?
    u64 top_level_pid = find_top_level_pid(pid);
    if (!top_level_pid) { return 0; }

    struct process_activity_info_t *val, zero = {};
    zero.read_bytes = 0;
    zero.read_total = 0;
    zero.written_bytes = count;
    zero.written_total = 1;
    zero.block_reads = 0;
    zero.block_writes = 0;

    val = process_activity.lookup_or_init(&top_level_pid, &zero);
    if (val) {
        val->written_bytes += count;
        val->written_total++;
    }

    return 0;
}

static  int is_write(struct request *req) {
    /*
    * The following deals with a kernel version change (in mainline 4.7, although
    * it may be backported to earlier kernels) with how block request write flags
    * are tested. We handle both pre- and post-change versions here. Please avoid
    * kernel version tests like this as much as possible: they inflate the code,
    * test, and maintenance burden.
    */
    #ifdef REQ_WRITE
        return !!(req->cmd_flags & REQ_WRITE);
    #elif defined(REQ_OP_SHIFT)
        return !!((req->cmd_flags >> REQ_OP_SHIFT) == REQ_OP_WRITE);
    #else
        return !!((req->cmd_flags & REQ_OP_MASK) == REQ_OP_WRITE);
    #endif
}

int trace_pid_start(struct pt_regs *ctx, struct request *req) {
    u64 pid = current_pid();

    // Is this block_io from a process we care about?
    u64 top_level_pid = find_top_level_pid(pid);
    if (!top_level_pid) {
        #ifdef PERF_TOOLS_DEBUG
        bpf_trace_printk("blk_account_io_start: ignoring unrelated bio pid=%d\n", pid);
        #endif

        return 0;
    }

    #ifdef PERF_TOOLS_DEBUG
    bpf_trace_printk("blk_account_io_start: related bio pid=%d\n", pid);
    #endif

    int read_inc, write_inc;
    if (is_write(req)) {
        read_inc = 0;
        write_inc = 1;
    } else {
        read_inc = 1;
        write_inc = 0;
    }

    struct process_activity_info_t *val, zero = {};
    zero.read_bytes = 0;
    zero.read_total = 0;
    zero.written_bytes = 0;
    zero.written_total = 0;
    zero.block_reads = read_inc;
    zero.block_writes = write_inc;

    val = process_activity.lookup_or_init(&top_level_pid, &zero);
    if (val) {
        val->block_reads += read_inc;
        val->block_writes += write_inc;
    }

    return 0;
}

